// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "CGLFW",
    products: [
        .library(name: "CGLFW", targets: ["CGLFW"]),
    ],
    targets: [
      .systemLibrary(
        name: "CGLFW",
        path: "Sources",
        pkgConfig: "glfw3"
      )
    ]
)
